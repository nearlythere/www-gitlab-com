---
layout: markdown_page
title: "Marketing Operations"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Marketing Operations

Marketing Operations (Mktg OPS) supports the entire Marketing team as well as other teams within GitLab. Mktg OPS works closely with Sales Operations to ensure information between systems is seamless and we are using consistent terminology in respective systems. 	


## Tech Stack  

For information regarding the tech stack at GitLab, please visit the [Business Operations section](/handbook/business-ops#tech-stack) of the handbook where we maintain a comprehensive table of the tools used across the Marketing, Sales, and Customer Success functional groups.  


## Working with Marketing OPS

**_The Mktg OPS Motto: If it isn't an Issue, it isn't OUR issue._**

Mktg OPS uses an [issue board](https://gitlab.com/groups/gitlab-com/-/boards/825719) this is a global issue board and will capture any issue in any group/sub-group in the repo.   

Labels to use:  
- `MktgOPS`: Issue initially created, used in templates, the starting point for any label that involved Mktg OPS
- `MktgOPS - FYI`: Issue is not directly related to operations, no action items for Mktg OPS but need to be aware of the campaign/email/event
- `MktgOPS - Action Needed`: Issue has a specific action item for Mktg OPS to be completed with delivery date 90 days or less from issue creation date. This tag might be used on projects/issues **not** owned by Mktg OPS (example: list upload).
- `MktgOPS - Future Action Needed`: Issue has a specific action item for Mktg OPS, the project/issue **is not** owned by Mktg OPS and delivery or event date is 90 days or more from issue creation. 
- `MktgOPS - On Hold`: Issue that is not within existing scope of Mktg OPS current targets, waiting on another team or department, any other blocker
- `MktgOPS - Planning`: Issue that does not directly fit into Mktg OPS focus and is being evaluated if it will replace
- `MktgOPS - In Process`: Issues that are not a top priority but have had worked started on them
- `MktgOPS - Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by Mktg OPS leadership. This category will be limited because not everything can be a priority.
- `MktgOPS - Complete`: Issue that involved operations and/or direct action item/tasks Mktg OPS is responsible for has been completed; issue that have been worked on and are done (should also be a closed issue)   

Mktg OPS also has a [**Marketing only** issue board](https://gitlab.com/gitlab-com/marketing/general/boards/331422) that captures the items within the [general Marketing group](https://gitlab.com/gitlab-com/marketing/general), this board also uses the same global labels above.      


## How-tos & FAQs

### Webcast Setup   
When creating a live or ondemand webcast it is important to integrate the event program across the platforms/programs used - GitLab Marketing site (`about.gitlab.com`), Marketo (Marketing Automation), Zoom (Webcast video software) and Salesforce (CRM). This provides transparency about the webcast to anyone with access to Salesforce, namely the Sales and Marketing teams.  

For a comprehensive overview on how to set up a webcast, please visit the [Business Operations section](/handbook/business-ops#Webinars) of the handbook.  

## Marketing Expense Tracking

| GL Code | Account Name | Purpose |
| :--- | :--- | :--- |
| 6100 | Marketing|Reserved for Marketing GL accounts|
| 6110 | Marketing Site|All software subscriptions, agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising|All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events|All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6140 | Email|All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand|All PR, AR, content, swag and branding costs |
| 6160 | Prospecting|All costs related to prospecting efforts |

## Campaign Cost Tracking
Marketing Program Managers track costs associated with campaigns - such as events, content, webcasts, etc. - by working with the Finance team to create custom tags. These tags can be applied to Expensify reports, corporate credit card charges, and vendor bills processed by Accounts Payable. Campaign expenses that are incurred by independent contractors should be clearly noted and included in their invoices to the company.

**The Marketing Programs Team follows the steps below to create and use campaign tags:**

1. MPM receieves confirmation from responsible team (i.e. Field Marketing, Content, etc.) that the budget for the campaign has been approved.
2. MPM sends an email request to the Accounts Payable including the requested tag, which typically follows the name of the Salesforce campaign.
3. Finance opens the tag in NetSuite and Expensify, notifying the requester when the tag is live.
4. Finance alerts the MPM (or requester) that the tag has been created.
5. MPM updates the related issue description with the tag name

**Things to Note:**

* All costs, including attendee travel expenses, must be tagged in order to capture the true cost of campaigns.
* Tagging expenses that are processed by Accounts Payable require Marketing to provide explicit instruction on when to apply tags, which should happen during the normal course of reviewing and approving vendor invoices.
* For expenses that are not campaign related, include a note to Accounts Payable clearly stating that campaigns are not applicable to the expense. This is required to ensure all campaign expenses are tagged.


## Email Management

Email database management is a core responsibility for Mktg OPS. Ensuring GitLab is following email best practices, in compliance with Global spam laws and overall health of active database are all priorities.   

Email creation, campaigns, follow up reporting and sending is the responsibility of the Marketing Program Managers. To request an email of any kind, please see the [instructions](/handbook/business-ops/#requesting-an-email) in the Business OPS section of the handbook.


### Email Communication Policy  

At GitLab, we strive to communicate with people in a way that is beneficial to them, most of our email marketing communications follow an explicit opt-in policy, although at times, we will communicate via email to people who have not explicitly opted-in. We do this to offer something of value (ex. an invite to a workshop, dinner, the opportunity to meet an industry leader, etc. Not an email inviting you to read a blog post) to the person. We always include the unsubscribe link in our communications and we respect the unsubscribe list. In addition to the unsubscribe button at the bottom of all of our emails, we  have available our [Email Subscription Center](/company/preference-center/), where people can control their email communication preferences. There are currently 4 [email segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments).

### Email Segments

Database segments and how someone subscribes to specific segment:  

- **Newsletter**: Users can [subscribe to the newsletter](/company/contact/) through the blog, Contact us page, and CE download page.
- **Security Alerts**: [Subscribe to security notices](/company/contact/#security-notices) on the GitLab Contact us page.
- **Webcasts**: When someone registers to a live or on-demand webcast
- **Live Events**: When someone registers to attend a live event, meet up or in-person training. Use of this segment is narrowed down by geolocation so notification and invitation emails are specific to related area.  

### Types of Email

**Breaking Change Emails**  
These are operation emails that can be sent on a very selective as needed basis. This is an operational-type email that overrides the unsubscribe and does not provide the opportunity for someone to opt-out. Usage example: GitLab Hosted billing change, Release update 9.0.0 changes, GitLab Page change and Old CI Runner clients.
It is very important to have Engineering and/or Product team (whoever is requesting this type of email) help us narrow these announcements to the people that actually should be warned so we are communicating to a very specific targeted list.

**Newsletter**  
Sent bi-monthly (every 2 weeks). Content Team is responsible for creating the content for each Newsletter.  

**Security Alerts**  
Sent on an as needed basis containing important information about any security patches, identified vulnerabilities, etc related to the GitLab platform. These emails are purely text based.

**Webcasts**   
Invitation and/or notification emails sent about future webcasts.   

**Live Events**   
Invitation emails to attend a live event (VIP or Executive Lunch), meet-up, or in-person training. These emails are sent to a geo-locational subset of the overall segment. This type of email is also used when we are attending a conference and want to make people aware of any booth or event we may be holding and/or sponsoring.


## Website Form Management   

The forms on about.gitlab are embedded Marketo forms. Any changes to the fields, layout, labels and CSS occur within Marketo and can be pushed live without having to make any changes to the source file on GitLab. When needing to change or embed a whole new form, ping Marketing OPS on the related issue so appropriate form and subsequent workflows can be created.   

Each Marketo form should push an event after successful submission. The following line goes above `return false` in the form embed code. Please update the event label from `demo` to reflect where the form was completed.

```
dataLayer.push({event: 'demo', mktoFormId: form.getId()});
```

In the event Marketo has an outage and/or the forms go offline the forms with highest usage/visibility (Free Trial and Contact Us) have been recreated as Google forms that can be embedded on the related pages as a temporary measure to minimize any effect till the outage is past.
