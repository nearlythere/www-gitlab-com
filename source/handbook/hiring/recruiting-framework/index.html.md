---
layout: markdown_page
title: "GitLab Recruiting Process Framework"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework
{: #framework}

**Purpose**: The Recruiting Process Framework was established to provide guidance on the recruiting process for the recruiting team, hiring manager, and hiring team to efficiently hire top talent and create a positive candidate experience.

**Variations**: It’s understood that variations to the process may occur given particular situations that make sense to a specified hiring team or need.

The communicated Recruiting Process Framework specifically focuses on candidates who are moving forward. It is understood that candidates who will be [declined will be done so in a timely and respectful manner](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates).

## Full Process

[![Diagram of full recruiting process framework](/images/handbook/recruiting/recruiting-process-framework.jpg)](https://docs.google.com/presentation/d/1OaLZPqKXNdmAvvkBVgAbHTs-Mhkp8XL6hOTKvaMhzsU/edit#slide=id.p)

| Hiring Manager ([HM](/handbook/hiring/recruiting-framework/hiring-manager)) | Recruiter ([R](/handbook/hiring/recruiting-framework/recruiter)) | Sourcer ([S](/handbook/hiring/recruiting-framework/sourcer)) | Coordinator ([C](/handbook/hiring/recruiting-framework/coordinator)) |
|:--------------:|:---------:|:-------:|:-----------:|
| [Step 1/HM: Identifying hiring need](/handbook/hiring/recruiting-framework/hiring-manager#step-1hm-identifying-hiring-need) | | |
| [Step 2/HM: Create vacancy in Greenhouse](/handbook/hiring/recruiting-framework/hiring-manager#step-2hm-create-vacancy-in-greenhouse) | | | [Step 3/C: Post vacancy, check details](/handbook/hiring/recruiting-framework/coordinator#step-3c-post-vacancy-check-assignment-details) |
| | [Step 4/R: Identify sourcing needs](/handbook/hiring/recruiting-framework/recruiter#step-4r-identify-sourcing-needs) | |
| | [Step 5/R: Schedule intake](/handbook/hiring/recruiting-framework/recruiter#step-5r-schedule-intake) | | |
| | [Step 6/R: Complete intake](/handbook/hiring/recruiting-framework/recruiter#step-6rcomplete-intake) | | |
| | [Step 7/R: Find top talent, schedule screens](/handbook/hiring/recruiting-framework/recruiter#step-7r-find-top-talent-schedule-screens) | [Step 8/S: Source top talent, schedule screens](/handbook/hiring/recruiting-framework/sourcer#step-8s-source-top-talent-schedule-screens) | |
| | [Step 9/R: Send assessment, if applicable](/handbook/hiring/recruiting-framework/recruiter#step-9r-send-assessment-if-applicable) | | |
| | [Step 10/R: Schedule top talent with hiring manager](/handbook/hiring/recruiting-framework/recruiter#step-10r-schedule-top-talent-with-hiring-manager) | | [Step 11/C: Schedule first interview](/handbook/hiring/recruiting-framework/coordinator#step-11c-schedule-first-interview) |
| [Step 12/HM: Complete feedback in Greenhouse/next steps](/handbook/hiring/recruiting-framework/hiring-manager#step-12hm-complete-feedback-in-greenhousenext-steps) | [Step 13/R: Team interviews](/handbook/hiring/recruiting-framework/recruiter#step-13r-team-interviews) | | [Step 14/C: Schedule team interviews](/handbook/hiring/recruiting-framework/coordinator#step-14c-schedule-team-interviews) |
| [Step 15/HM: Hiring team to complete feedback in Greenhouse](/handbook/hiring/recruiting-framework/hiring-manager#step-15hm-hiring-team-to-complete-feedback-in-greenhouse) | [Step 16/R: Host interview team debrief if applicable](/handbook/hiring/recruiting-framework/recruiter#step-16r-host-interview-team-debrief-if-applicable) | | |
| | [Step 17/R: Connect with hiring manager on next steps](/handbook/hiring/recruiting-framework/recruiter#step-17r-connect-with-hm-on-next-steps) | | |
| | [Step 18/R: Request references](/handbook/hiring/recruiting-framework/recruiter#step-18r-request-references) | | |
| [Step 19/HM: Complete references](/handbook/hiring/recruiting-framework/hiring-manager#step-19hm-complete-references) | | | [Step 20/C: Initiate background check](/handbook/hiring/recruiting-framework/coordinator#step-20c-initiate-background-check) |
| | [Step 21/R: Submit offer](/handbook/hiring/recruiting-framework/recruiter#step-21r-submit-offer) | | |
| | [Step 22/R: Verbalize offer](/handbook/hiring/recruiting-framework/recruiter#step-22r-verbalize-offer) | | [Step 23/C: Send contract](/handbook/hiring/recruiting-framework/coordinator#step-23c-send-contract) |
