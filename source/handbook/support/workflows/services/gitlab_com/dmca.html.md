---
layout: markdown_page
title: DMCA Removal Requests
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

# Overview
This worklow stipulates the steps that need to be taken when receiving DMCA take down requests for users who host copyrighted content belonging to others. Follow the guide below on how to handle incoming requests for GitLab.com.

**DMCA complaints have a 24 hour Service Level Agreement.**

## Workflow

### First 24 hours
NOTE: **Note:** If the DMCA is originating from Google, skip to the [Valid Notice](#valid-notice) section below.

1. Review the notice, if the request was not received from Legal or Abuse, please use the `DMCA::Acknowledge Receipt` macro, and cc legal and abuse-reports for them to review the request. The macro should add the appropriate tag and put the ticket on-hold for 24 hours.
1. Legal will review the notice, then add an internal note on the original ticket advising whether it is a valid notice, and set the ticket as _Open_. If the notice is invalid, Legal will response to the original requester.

#### Valid Notice

1. Forward the notice to the defendant in a new ticket using the `DMCA::Notice to Content 'Owner'::First Touch` macro. Set the requester email to the email address(es) belonging to the GitLab.com user and paste the content of the forwarded notice into the space provided. The macro should set the ticket on-hold and re-open in 24 hours.
1. In the content owner ticket, also add an internal note referencing the original ticket.
1. In the original ticket, use the macro `DMCA::Notice to Legal::First Touch` and add the link to the owner contact ticket.

### Next 24 hours

1. Confirm whether the content is still live. If the content has been removed, add an internal note and resolve the ticket.
1. Respond to the original requester using the `DMCA::Requestor reply takedown complete` macro.  
1. If the content is still live and the user has not responded, proceed to send a follow up notice to them using the `DMCA::Notice to Content 'Owner'::Second Touch` macro.
1. On the original requester ticket, use the ``DMCA::Notice to Legal::Second Touch` macro.

### Taking Action
If the content still remains active and the user has not responded to the notice:

1. Pull up the projects in question and make them private, then block the user.
1. Ensure that your internal note indicates the original state of the projects in question. --> `Were the projects already private and which users had access?`
1. Reach out to the Abuse Team who will review the ticket and take the appropriate action. See [Abuse Team](#abuse-team).

## User disputes a takedown notice
If the user disputes the takedown request, the user should have submited a valid counter-notice.

1. Use the `DMCA::Acknowledge Counter Receipt` macro and cc Legal to review.
1. If the counter-notice is determined to be valid, Legal will add an internal note whether it is a valid notice.

### Valid Counter-Notice

1. If the counter-notice is valid and vetted by Legal, forward the notice on to the plaintiff (original requester).
1. Still follow the steps in the [Taking Action](#taking-action) section above even though the owner has responded.

### Invalid Counter Notice

1. If the counter-notice is invalid after being vetted by Legal, proceed with steps stipulated in [Taking Action](#taking-action).

## Abuse Team
All information in the rest of this workflow is for the _Abuse Team_ only.

### Action requested by Support

1. Review the notice and steps followed. Confirm that the project(s) in question have been made private and the user has been blocked.  
1. Send another follow-up to the defendant, this time informing them that action has been taken, using the macro `DMCA::Notice to Content 'Owner'::Final Touch (takedown)`.
1. In the original ticket, use the `DMCA::Notice to Legal::Final Touch` macro and add any additional relevant information.

### Reactivating the content
If there was a valid counter-notice and no response has been received from the plaintiff within 10 days of the counter-notice being forwarded, proceed witht the following steps:

1. Pull up the projects in question returning them to the original state and account.  Refer to notes made in step 2 under [Taking Action](#taking-action).
1. Add an internal note to the defendant and plaintiff tickets leaving it resolved.
