---
layout: markdown_page
title: Dormant Username Policy
category: Support Workflows
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

### Overview
As per the [statement of support](/support/#dormant-username-requests), dormant namespaces can be released when they meet the appropriate criteria.

**NOTE:** When applying any of the macros ensure to replace the placeholder **“REQUESTEDUSERNAME”** with the username requested.
______________

### Workflow

1. Search for the requested username in [GitLab.com admin](https://gitlab.com/admin/users), once found visit the user's GitLab admin profile.
1. Apply the **Account::Dormant Username::Internal Checklist** macro in Zendesk.
1. Answer all questions in the **Internal Checklist** (Yes/No) ensuring to cross-check the information found in the admin section.
1. If the username is eligible for immediate release, follow [Request successful](#request-successful).
1. If the username is eligible for release, follow [Username is available](#username-is-available).
1. If the username is not eligible for release, follow: [Username is not available](#username-is-not-available).

### Username is available

Contact Owner:

1. Create a **new Zendesk ticket** with the **username owner's email address** (found in admin).
1. Apply the **Account::Dormant Username::Contact Username Owner** macro and mark the ticket as **On-hold**.
1. Make an internal comment providing a link to the **username requesters ticket**.

Requester's Ticket:

1. Copy the ticket's link and add it to the **Internal Checklist**.
1. Reply to the requester with the **Account::Dormant Username::First Response** macro and mark ticket as **On-hold**.

#### Username owner responded
If the username owners makes a response (don’t remove my username) follow these steps:

1. Apply the **“Account::Dormant Username::Cancel Request”** Macro to the **username owners response**.
1. Apply the **“Account::Dormant Username::Failed Username Request”** to the **username requesters ticket**.

#### Username owner has not responded
If after one week and there has been no response, apply the **Account::Dormant Username::Contact Username Owner** macro a second time and mark the ticket as **On-hold**.

After two weeks, the ticket will be **automatically marked as open and an email sent to the assigned agent**.

If the username owner makes no response, follow the [Request successful](#request-successful) steps.

### Request successful
If the request is successful, follow these steps:

In admin, rename the owner's username:

1. Navigate to the username in admin - https://gitlab.com/admin/users
1. Select “Edit” on the user's profile.
1. Append “_idle” to the user’s username.
1. Save changes.

In Zendesk:

1. Apply the **“Account::Dormant Username::Successful Username Request”** macro to the **username requesters ticket** and mark the ticket as **Solved**.

### Username is not available

1. Apply **"Account::Dormant Username::Failed Username Request"** macro and mark ticket as **Solved**.

### FAQs

1. Does a login in response to dormant request mean that the account is active? No, the user has to explicitly reply to the dormant request saying "I want to keep my username". If the user hasn't responded and has just logged in, send a final message saying something like, "I see you logged in at X, but you need to let us know here if you want to keep your username".
1. What constitutes data in the account? A group, a project, etc. means data. Even if the project or group is empty.

__________________

**Macros**

* [Account::Dormant Username::Failed Username Request](https://gitlab.zendesk.com/rules/94534768/edit)
* [Account::Dormant Username::Internal Checklist](https://gitlab.zendesk.com/rules/93505588/edit)
* [Account::Dormant Username::First Response](https://gitlab.zendesk.com/rules/94687707)
* [Account::Dormant Username::Contact Username Owner](https://gitlab.zendesk.com/rules/94531288/edit)

**Automations**

* [Dormant Username Check](https://gitlab.zendesk.com/rules/94693587/edit)
