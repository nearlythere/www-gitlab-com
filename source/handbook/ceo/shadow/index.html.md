---
layout: markdown_page
title: "CEO shadow program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

The CEO shadow program at GitLab is not a job title but a temporary assignment to shadow the CEO.
The shadow(s) will be present at all meetings of the CEO.

## Goal

The goal of the shadow program is to give current and future [directors and senior leaders](/company/team/structure/) at GitLab a overview of all aspects of the company.
This should lead to leadership who are able to do [global optimizations](/handbook/values/#results).

## Attendence

You will attend all meetings of the CEO, this includes:

1. 1-1s with reports
1. interviews with applicants
1. conversations with board members

These meetings can have different formats:

1. video calls
1. in-person meetings.
1. dinners that are business related
1. customer visits
1. conferences

You will live in San Francisco for the period and travel with the CEO. 

You will not attend when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

This is probably the most open program in the world.
It depends on the participants respecting confidentiality, during the program, after the program, and after they leave GitLab.

## Rotation

We want many people to be able to benefit from this program, therefore we rotate often.
It is also important that an incoming person is trained so that the management overhead can be light.
So we'll do a See One, Do One, Teach One like in the medical profession.

A rotation is three weeks:

1. See one, you are trained by the outgoing person.
1. Do one, you perform the tasks.
1. Teach one, you train the incoming person.

In general there should be no planned holidays during the shadowing during the period unless the CEO is also on holiday.

## Tasks

Since a rotation is over a short period there are no long running tasks that you can assume.

The tasks consist of short term tasks, for example:

1. Make handbook updates
1. Draft email responses
1. Solve urgent issues, for example a complaint from a customer or coordinating the response to a technical issue.
1. Prepare meetings.
1. Take notes during meetings.
1. Follow up on meetings (improve the notes, followup appointment, thank you cards, swag).
1. Ensure that internal teams are updated with action items and outcomes
1. Compile a report on a subject.
1. Write a blog post based on a conversation.
1. Editing a recorded video.
1. Promoting the work you created.
1. Writing a blog post about something you learned or your experience.
1. Doing the camerawork for a video.
1. Control slides and muting during the board meeting.
1. Setting up for an in-person meeting.
1. Fetching drinks or other things.
1. Add to the documentation of GitLab.
1. Add training for this program to the handbook.
1. Training the incoming person.

A ongoing shadow program with a fast rotation is much more time consuming for the CEO then a temporary program or a rotation of a year or longer.
Therefore a quick turnaround on documentation is of paramount importance.

## Naming

For now this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered are:

1. Technical assistant. Seems confusing with [executive assistant](/job-families/people-ops/executive-assistant/). ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff. "](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy)
1. Chief of Staff. This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently. The executive assistants reports to peopleops.
1. [Global Leadership Shadow Program](https://www2.deloitte.com/ng/en/pages/careers/articles/leadership-shadow-program.html) is too long if only the CEO is shadowed.

## Eligible

If you are a current or future [directors and senior leaders](/company/team/structure/) at GitLab you can apply.
For new team members this might be the first thing they do after completing our [onboarding](/handbook/general-onboarding/).
To apply for the program please ask your manager.
