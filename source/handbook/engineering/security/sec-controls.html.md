---
layout: markdown_page
title: "GitLab Security Compliance Controls"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Security Controls

Adobe's [open source compliance framework](https://blogs.adobe.com/security/2017/05/open-source-ccf.html) served as the starting point of GitLab's overarching information security framework. It has been adapted and expanded as needed and the result is the below list of controls grouped by families and sub-families. We are currently working towards providing the following for each control in the below table:
* Control statement
* Context
* Scope
* Ownership
* Spirit/Intent of the control
* Implementation guidance
* Examples of evidence an auditor might request to satisfy this control

# List of controls by family:

## Asset Management
* Device and Media Inventory
    * Inventory Management
    * Inventory Management: Payment Card Systems
    * Inventory Labels
* Device and Media Transportation
    * Asset Transportation Authorization
    * Asset Transportation Documentation
* Component Installation and Maintenance
    * Maintenance of Assets
    * Tampering of Payment Card Capture Devices

## Backup Management
* Backup
    * Backup Configuration
    * Resilience Testing
    * Alternate Storage

## Business Continuity
* Business Continuity Planning
    * Business Continuity Plan
    * Continuity Testing
    * Business Impact Analysis

## Change Management
* Change Management
    * Change Management Workflow
    * Change Approval
* Segregation of Duties
    * Segregation of Duties

## Configuration Management
* Baseline Configurations
    * Baseline Configuration Standard
    * Default "Deny- all" Settings
    * Configuration Checks
    * Configuration Check Reconciliation: CMDB
    * Time Clock Synchronization
    * Time Clock Configuration Access
    * Default Device Passwords
    * Process Isolation

## Data Management
* Data Classification
    * Data Classification Criteria
* Choice and Consent
    * Terms of Service
* Data Handling
    * External Privacy Inquiries
    * Test Data Sanitization
* Data Encryption
    * Encryption of Data in Transit
    * Encryption of Data at Rest
    * Approved Cryptographic Technology
* Data Storage
    * Credit Card Data Restrictions
    * Personal Account Number Data Restrictions
* Data Integrity
    * Changes to Data at Rest
* Data Removal
    * Secure Disposal of Media
    * Customer Data Retention and Deletion
* Social Media
    * Social Media

## Identity and Access Management
* Logical Access Account Lifecycle
    * Logical Access Provisioning
    * Logical Access De-provisioning
    * Logical Access Review
    * Role Change: Access De- provisioning
    * Shared Logical Accounts
    * Shared Account Restrictions
* Authentication
    * Unique Identifiers
    * Password Authentication
    * Multifactor Authentication
    * Authentication Credential Maintenance
    * Session Timeout
    * Account Lockout: Cardholder Data Environments
    * Full Disk Encryption
* Role-Based Logical Access
    * Logical Access Role Permission Authorization
    * Source Code Security
    * Service Account Restrictions
    * PCI Account Restrictions
* Remote Access
    * Virtual Private Network
    * Ability to Disable Remote Sessions
    * Remote Maintenance: Authentication Sessions
    * Remote Maintenance: Unique Authentication Credentials for each Customer
* End-user Authentication
    * End-user Environment Segmentation
* Key Management
    * Key Repository Access
    * Data Encryption Keys
    * Key Maintenance
    * Clear Text Key Management
* Key Storage and Distribution
    * Key Store Review
    * Storage of Data Encryption Keys
    * Clear Text Distribution

## Incident Response
* Incident Response
    * Incident Response Plan
    * Incident Response Testing
    * Incident Response
* Incident Communication
    * External Communication of Incidents
    * Incident Reporting Contact Information
    * Incident External Communication

## Mobile Device Management
* Mobile Device Security
    * Configuration Management: Mobile Devices

## Network Operations
* Perimeter Security
    * Network Policy Enforcement Points
    * Inbound and Outbound Network Traffic: DMZ Requirements
    * Ingress and Egress Points
    * Non-disclosure of Routing Information
    * Dynamic Packet Filtering
    * Firewall Rule Set Review
* Network Segmentation
    * Network Segmentation
    * Card Processing Environment Segmentation
* Wireless Security
    * Disable Rogue Wireless Access Points
    * Wireless Access Points
    * Rogue Wireless Access Point Mapping
    * Authentication: Wireless Access Points

## People Resources
* On-boarding
    * Background Checks
    * Performance Management
* Off-boarding
    * GitLab Property Collection
* Compliance
    * Disciplinary Process

## Risk Management
* Risk Assessment
    * Risk Assessment
    * Continuous Monitoring
    * Self- Assessments
    * Service Risk Rating Assignment
* Internal and External Audit
    * Internal Audits
* Controls Implementation
    * Remediation Tracking
    * Statement of Applicability

## Security Governance
* Policy Governance
    * Policy and Standard Review
    * Exception Management
* Security Documentation
    * Information Security Program Content
* Privacy Program
    * Privacy Readiness Review
* Workforce Agreements
    * Proprietary Rights Agreement
    * Review of Confidentiality Agreements
    * Key Custodians Agreement
* Information Security Management System
    * Information Security Management System Scope
    * Security Roles and Responsibilities
    * Security Roles and Responsibilities: PCI Compliance
    * Information Security Resources

## Service Lifecycle
* Release Management
    * Service Lifecycle Workflow
* Source Code Management
    * Source Code Management

## Site Operations
* Physical Security
    * Secured Facility
    * Physical Protection and Positioning of Cabling
* Physical Access Account Lifecycle
    * Provisioning Physical Access
    * De-provisioning Physical Access
    * Periodic Review of Physical Access
    * Physical Access Role Permission Authorization
    * Monitoring Physical Access
    *Surveillance Feed Retention
    * Visitor Access
* Environmental Security
    * Temperature and Humidity Control
    * Fire Suppression Systems
    * Power Failure Protection

## Systems Design Documentation
* Internal System Documentation
    * System Documentation
    * System Documentation: Cardholder Environment
* Customer- facing System Documentation
    * Whitepapers

## Systems Monitoring
* Logging
    * Audit Logging
    * Secure Audit Logging
    * Audit Logging: Cardholder Data Environment Activity
    * Audit Logging: Cardholder Data Environment Event Information
    * Audit Logging: Service Provider Logging Requirements
    * Log Reconciliation: CMDB
    * Audit Log Capacity and Retention
    * Enterprise Antivirus Logging
* Security Monitoring
    * Security Monitoring Alert Criteria
    * Log-tampering Detection
    * Security Monitoring Alert Criteria: Failed Logins
    * Security Monitoring Alert Criteria: Privileged Functions
    * Security Monitoring Alert Criteria: Audit Log Integrity
    * Security Monitoring Alert Criteria: Cardholder System Components
    * System Security Monitoring
    * Intrusion Detection Systems
* Availability Monitoring
    * Availability Monitoring Alert Criteria
    * System Availability Monitoring

## Third Party Management
* Vendor Assessments
    * Third Party Assurance Review
    * Vendor Risk Management
    * Forensic Investigations
* Vendor Agreements
    * Network Access Agreement: Vendors
    * Vendor Non- disclosure Agreements
    * Cardholder Data Security Agreement
    * Network Service Level Agreements (SLA)
* Vendor Procurement
    * Approved Service Provider Listing

## Training and Awareness
* General Awareness Training
    * General Security Awareness Training
    * Code of Conduct Training
* Role-Based Training
    * Developer Security Training
    * Payment Card Processing Security Awareness Training

## Vulnerability Management
* Production Scanning
    * Vulnerability Scans
    * Vulnerability Assessment: Cardholder Data Environment 
    * Approved Scanning Vendor
* Penetration Testing
    * Application Penetration Testing
    * Penetration Testing: Cardholder Data Environment
* Patch Management
    * Infrastructure Patch Management
* Malware Protection
    * Enterprise Antivirus
    * Enterprise Antivirus Tampering
* Code Security
    * Code Security Check
    * Code Security Check: Cardholder Data Environment
* External Advisories and Inquiries
    * External Information Security inquiries
    * External Alerts and Advisories
* Program Management
    * Vulnerability Remediation