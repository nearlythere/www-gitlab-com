---
layout: job_family_page
title: "Technical Marketing Manager"
---

As a technical marketing manager, you will work closely with our marketing, engineering, product, business development and sales team, and partners to help everyone understand how core GitLab technical value offerings solve customer problems as well as educate them about market competitors.  You will be responsible for technical marketing content and representing GitLab as a technical evangelist at key trade shows and customer meetings.

## Requirements

* In-depth industry experience and working knowledge using or building software products, particularly cloud software, collaboration software, or developer tools.
* 2-3 years experience developing demos, webinars, videos, technical collateral, ROI/value oriented assessments, etc.
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Experience with Agile and DevOps methodologies across the software development lifecycle. Preferably in a large enterprise environment.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Obsessive about iteration, quality, clarity, details, & execution
* You share our [values](/handbook/values), and work in accordance with those values.

## Senior Requirements

* 4-5 years experience independently developing demos, webinars, videos, technical collateral, ROI/value oriented assessments, etc.
* At least 6 years enterprise software marketing experience, including 4 directly in product marketing
* In-depth industry experience with Agile and DevOps methodologies across the software development lifecycle. Preferably in a large enterprise environment.
* Experience designing sales collateral from scratch based on sales conversations, sales calls, product interviews, user interviews, market research, and your own experience.
* Experience designing and delivering sales enablement training to ensure that salespeople can confidently and accurately deliver GitLab's enterprise value proposition.

## Specialities

### Cloud Native

* Experience with container technologies (such as Kubernetes, Docker), virtualization technologies, and cloud application architectures (such as Microservices).
* Past experience working on Cloud Computing platforms such as GCP, AWS, and Azure.
* Experience with serverless computing.


### Security

* At least 2-3 years of product marketing experience marketing cybersecurity products and services such as application security, end point security, SIEM, security analytics or network security domains.
* Understand market dynamics, the competition, and customer needs of the cybersecurity space.
* Train and enable sales and partner teams to understand GitLab security features and capabilities of the product.

### Competitive Intelligence

* Prior experience in competitive intelligence, product management, technical product marketing or solution architect roles.
* Manage competitive strategy and analysis program for GitLab through deep research and analysis of competitor offerings, roadmaps, strategy, messaging, and go to market strategies.
* Work and collaborate with different teams including product management, engineering, marketing, and sales teams to analyze and identify competitive products strengths, weaknesses, and differentiators.
* Provide competitive intelligence training, support and consulting to sales, product, marketing, and partner teams.
* Provide core competitive content to marketing and sales teams to support competitive take-out campaigns.
* Develop and maintain competitive landscape content including web site pages, presentations, demos, and other assets.


## Responsibilities

* Install, configure, evaluate, and analyze competitive and complementary technologies to compare and differentiate them with GitLab’s offerings
* Maintain competitive information repository and keep it up to date.
* Develop technical collateral such as demos, webinars, videos, competitive battle cards, ROI tools, and assist with customer Requests For Proposals (RFPs).
* Represent GitLab as a technical evangelist at trade shows and customer meetings
* Demonstrate GitLab value across the entire Software Development Lifecycle, especially in large enterprise environments.

## Senior Responsibilities

* Train and enable sales and partner teams on a technical level to deeply understand GitLab features and capabilities of the product.
* Drive development of technical collateral such as demos, webinars, videos, competitive battle cards, ROI tools, and assist with customer Requests For Proposals (RFPs).
* Deliver technical insight about GitLab products and competitor products to product management to assist in product direction.
* Provide technical information and demos for analyst reports, demos, presentations.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates then will be invited to schedule a 45 minute interview with a Product Marketing Manager
* Candidates may be invited to schedule 45 minute interview with a member of our Customer Success team.
* Candidates may be invited to schedule a 45 minute interview with a Product Manager.
* Candidates may be invited to schedule a 45 minute interview with an additional member of the Product Marketing Team
* Candidates will then be invited to schedule a 45 minute interview with our Director of Product Marketing
* Finally, our CEO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
