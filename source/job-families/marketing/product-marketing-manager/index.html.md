---
layout: job_family_page
title: "Product Marketing Manager"
---

## Requirements

* Technical background or good understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* 3-5 years of experience in product marketing, desired experience in the [specialty](#specialties).
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Excellent spoken and written English
* Proven ability to be self-directed and work with minimal supervision.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Uses data to measure results and inform decision making and strategy development.
* You share our [values](/handbook/values), and work in accordance with those values.

## Senior Requirements

* At least 6 years enterprise software marketing experience, including 4 directly in product marketing
* In-depth industry experience and working knowledge using or building software products, particularly cloud software, collaboration software, or developer tools
* Highly technical to be able to independently develop demos, webinars, videos, technical collateral, ROI/value oriented assessments, etc.
* Experience designing sales collateral from scratch based on sales conversations, sales calls, product interviews, user interviews, market research, and your own experience
* Experience in building sales enablement collateral and the corresponding training to ensure that salespeople can confidently and accurately deliver GitLab's enterprise value proposition.
* Experience with sales enablement in a partner & alliances context
* Experience managing sales and marketing requirements & expectations in a fast-growing environment
* Obsessive about iteration, quality, clarity, details, & execution
* Market research and competitive intelligence experience
* Stay on top of trends in the space via conferences, publications, social media, etc

## Specialties

### Security Specialist

As a Product Marketing Manager, Security Specialist, you will work closely with our marketing, engineering, business development and sales team, and partners to help them understand how GitLab security capabilities solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s security capabilities.

#### Responsibilities
* At least 2-3 years of product marketing experience marketing cybersecurity products and services such as application security, end point security, SIEM, security analytics or network security domains.
* Understand market dynamics, the competition, and customer needs of the cybersecurity space.
* Develop product marketing collateral such as website pages focused on security capabilities of GitLab, demos, webinars, videos, white papers, customer and partner facing decks, and data sheets.
* Train and enable sales and partner teams to understand GitLab security features and capabilities of the product.

#### Senior Responsibilities
* At least 5-7 years of product marketing experience marketing cybersecurity products and services such as application security, end point security, SIEM, security analytics or network security domains.
* Deep understanding of Agile and DevOps methodologies across the entire Software Development Lifecycle, specially in large enterprise environments.
* Understand enterprise buyer and user personas tasked with security challenges and pain points and how to market security solutions to them.
* Deliver technical insight about GitLab products and competitor products in the security space to product management to assist in product direction.
* Security certifications such as CISSP desired but not required.

### Monitoring Specialist

As a Product Marketing Manager, Monitoring, you will work closely with our marketing, engineering, business development and sales team, and partners to help them understand how GitLab monitoring capabilities solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s monitoring capabilities.

#### Responsibilities

* At least 2-3 years of product marketing experience marketing IT operations monitoring products and services such as tracing, logging, application performance monitoring, incident management, and related metrics.
* Understand market dynamics, the competition, and customer needs of the enterprise IT operations space.
* Develop product marketing collateral such as website pages focused on monitoring capabilities of GitLab, demos, webinars, videos, white papers, customer and partner facing decks, and data sheets.
* Train and enable sales and partner teams to understand GitLab monitoring features and capabilities of the product.


#### Senior Responsibilities

* At least 5-7 years of product marketing experience marketing IT operations monitoring products and services such as tracing, logging, application performance monitoring, incident management, and related metrics.
* Deep understanding of Agile and DevOps methodologies across the entire Software Development Lifecycle, especially in large enterprise environments.
* Understand enterprise buyer and user personas tasked with enterprise IT operations and monitoring challenges and pain points and how to market monitoring solutions to them.
* Deliver technical insight about GitLab products and competitor products in the monitoring space to product management to assist in product direction.
* Experience with cloud native, datacenter, and hybrid infrastructure and applications desirable.


### Enterprise Specialist

Do you have experience uncovering a product's core benefits, marketing to an enterprise buyer, or navigating a sales process? Product marketers are at the intersection of all teams.
GitLab is looking for a collaborative and process-driven product marketer to help craft our enterprise story, align to our sales process, and deliver effective go-to-market and sustain campaigns.

The Partner Product Marketing Manager role includes 4 key areas of focus:

* Enterprise-focused product messaging
* Enterprise-focused GTM
* Sales enablement
* Market research

#### Responsibilities

Enterprise Marketing:
* Conduct market research to understand buyer personas, competitive landscape, and other data to help influence marketing, sales, and product decisions.
* Own sales enablement materials and training.
* Work with product and engineering to craft the enterprise story for new and existing features.
* Work with sales, demand generation, and content marketing to create content and campaigns that is geared to our enterprise audience.
* Help technical writing and engineering to create documentation that address audience needs.

#### Senior Responsibilities

* Become product & market expert for GitLab, able to read trends and quickly iterate on messages our marketing & sales teams can use
* Contribute to launches for new product offerings
* Produce collateral such as presentations, videos, demos, whitepapers, one pagers, case studies, battle cards, etc.
* Train and enable sales and partner teams to deeply understand the buyer value proposition and features and capabilities of the product
* Support creation of enablement plans and deliver it to sales, sales development, solutions architects, and partners
* Capture how our products are used and the value created across industries and customers for collateral creation and customer reference marketing
* Collaborate with our sales development reps, account executives and partners to successfully drive the sales of GitLab’s product offerings
* Drive integrated communications initiatives with other teams across internal and partner ecosystems

### Partner and Channel Marketing

As the Partner Product Marketing Manager, you will work with our marketing, business development and sales teams, to develop and execute global go-to-market strategies and programs to drive customer acquisition and revenue growth for GitLab. Reporting to the Director, Product Marketing, you will have a global charter to enable partners and resellers within our growing partner and channel ecosystems. In addition, you’ll be responsible for crafting and implementing co-marketing campaigns targeting developers and IT professionals, to increase awareness and adoption of GitLab. You will work to shape go-to-market messaging and strategy for new offerings with partners and work with partners and resellers to build an effective and scalable ecosystem.

#### Responsibilities

Channel Marketing:
* Develop our worldwide channel marketing strategy, and train the field marketing team on regional execution that maximizes our reach and scale with channel partners.
* Implement channel marketing initiatives, programs, communication vehicles, and sales tools to drive increased market adoption and channel revenue goals.
* Spearhead enablement activity with resellers to drive end-user adoption, and manage MDF program.
* Assist sales team in the development of actionable and measurable programs for our channel partners.
* Work with field marketing to execute channel marketing programs, regionally.
* Accurately track activity performance and provide well-informed recommendations on future resource and budget allocation.
* Assist in the preparation of reports to help evaluate activity and reseller effectiveness, conversion rate, and relative performance.
* Market to, and through our channel. Ensure prospective resellers understand the value of partnering with GitLab.
* Complete other duties as assigned to meet company channel goals.

Partner Marketing:
* Initiate and develop co-marketing initiatives to execute in tandem with our partners.
* Market the benefit of our technology partnerships to all GitLab prospects.
* Develop and execute marketing programs when announcing new technology partnerships.
* Develop sales enablement programs for new partnerships, training the sales team on added functionality and associated benefits.
* Develop an ROI performance analysis for partner marketing initiatives and utilize findings to tailor future initiatives.
* Develop outreach plan for existing partnerships maintaining a steady development of joint content and demand generation activities.
* Create partnership positioning program for a cloud native approach and associated ecosystem.

#### Responsibilities

* Able to build strong relationships with partners globally.


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates then may be offered a 45 minute interview with our Chief Marketing Officer
* Next, candidates will be invited to schedule a interview with the Senior Director of Marketing and Sales
* Candidates will then be invited to schedule 45 minute interviews with our Regional Sales Director of the US East Coast
* After, candidates may be invited to schedule a 45 minute interview with the Vice President of Product
* Next, candidates will be invited to schedule a interview with the Chief Revenue Officer.
* Finally, our CEO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
