---
layout: job_family_page
title: "Reference Program Manager"
---

As our Customer Reference Manager, you will be responsible for creating and managing a compelling and persuasive customer reference program that provides timely references in support of sales, analyst relations, field events, press releases and other marketing activities.

## Requirements

* Prior experience working with customers (references) and influencers in B2B enterprise software and services required.
* Understanding of how to build relationships with volunteers in the reference program.
* Excellent project and time management skills.
* Excellent written and verbal communications skills.
* Self-starter with a strong sense of ownership.
* Able to prioritize in a complex, fast-paced and lean organization.
* Passion for building a world class analyst program and desire to own and refine key operational processes
* You share our [values](/handbook/values), and work in accordance with those values


## Responsibilities

* Manage the GitLab world wide customer reference program, working cross-functionally between sales, customer success and marketing to identify, engage, and develop reference customers.
* Manage high-touch customer reference relationships.
* Create and maintain in-depth customer profiles and contacts.
* Interface with sales to understand their needs and advise them on how/when to best use references.
* Provide input into marketing and sales deliverables including peer to peer networking, event speaking, 1: many reference calls.
* Manage reference requests tracking reference activity and outcomes.
* Report outcome metrics for strategic customer engagement, and foster and promote long-term, mutually beneficial customer relationships.
* Identify and track potential customer success stories from the reference program.
* Serve as Program Manager for GitLab Customer Advisory Boards.
* Help define customer reference metrics/goals/benchmarks and track/report progress against them.


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a series of 45 minute interviews with a Senior Product Marketing Manager
* Candidates will then be invited to schedule 45 minute interviews with our Regional Director of Sales-West.
* Then, Candidates will be invited to schedule 45 minute interviews with Director of Product Marketing.
* Finally, selected candidates may be asked to interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
