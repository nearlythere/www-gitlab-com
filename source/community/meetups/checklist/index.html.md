---
layout: markdown_page
title: GitLab Meetups Checklist
suppress_header: true
---

## On this page
{:.no_toc}

- TOC
{:toc}


## GitLab Meetups Checklist 

This guide is intended to help Meetup organizers run events that their community will love. Our aim is to be as comprehensive as possible to enable everyone to become an organizer, regardless of experience. We realize that we don't have all the answers so if you find anything incorrect, notice something missing, or identify other changes to be made, please [open an issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issue) for our team to review. Happy planning! 

### Overview 

GitLab supports community leaders who want to organize meetups and tech events in their cities and hometowns. As the first single application for the entire DevOps lifecycle, all events that discuss and educate on the software development lifecycle and developer experience are eligible for [GitLab support](#how-gitlab-can-help).

Our goal in supporting these events to increase awareness of GitLab and Concurrent DevOps and better educate the technology community about the power of our application.

### Who can contribute

At GitLab, we believe everyone can contribute. We support people who are interested in organizing events or growing existing communities. If you have experience organizing tech events or meetup groups, that is great - but it is not required. We're happy to work with first-time organizers, too.

The only requirements for organizers are a passion for GitLab and a belief in our [mission](https://about.gitlab.com/company/strategy/#mission).

### Why should you get involved

The benefits to organizing an event are as varied as the people who organize them. Everyone has their own reasons. That said, for tech events specifically, we have identified a few common threads that tie organizers together: 

- A passion for a specific technology and a desire to learn more about it
- An interest in connecting people 
- Support from GitLab's team of community experts

### How GitLab can help

GitLab supports meetup organizers with planning and logistics support, connections to speakers, GitLab swag, and financial support for food and beverages for your events.

## Planning a meetup 

### Getting started

- When you're ready to begin planning a meetup, please open an issue using our [meetups template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetups). 
- If you would like to review this guide and discuss your plans, please [schedule a Zoom call](https://calendly.com/jcoghlan) with GitLab's Evangelist Program Manager.

### What to do ASAP 

-  Find speakers. Need help? Use the GitLab [Find a Speaker](https://about.gitlab.com/events/find-a-speaker/) page. 
-  Find a venue. Cafes, community centers, coworking spaces, and local tech companies are common venues for meetups. 
-  Set the date. This requires confirming availability of both venue and speaker.
-  Set up an event page. We are happy to connect your group with our [Meetup page](https://www.meetup.com/pro/gitlab). Leave a comment on the event issue if you'd like to leverage our Meetup page to get your group started.  
-  Market the event to your target audience. We recommend Reddit, Twitter, LinkedIn, Facebook, and other social channels to reach your audience. Use appropriate hashtags to reach people outside your network of followers. You may also want to promote the event on tech mailing lists or websites that focus on your area. 

### What to do one week before your event

-  Confirm plans for the event with your speaker(s) and venue. This is a good time to discuss how the speakers plan to present. Will they be using their laptop? What type of ports does their laptop have? Will they need an adapter? Do these match what is available at the venue? 
-  Recruit volunteers from your network or community to help you on the day of the event. 
-  Send a reminder to your guests about the event and encourage them to help you promote it. Include a simple ask of "share this with your colleagues, friends, and on social media".

### What to do the day of your event

-  Send reminder to your guests before 1200 local time. Include directions, an agenda, and any other important information: is the entrance tucked away? will they need an ID or code to enter the building? are folks welcome to arrive early or is there a set time that doors will open? 
-  Print and hang signs directing attendees to the room where the event will be held and the restrooms.
-  Request volunteers arrive 30 min before the start time so you can brief them on their responsibilites and answer any questions. 
-  Test the AV in the room to ensure everything is in working order before guests begin to arrive. This will allow time to troubleshoot should any issues arise. It never hurts to have extra cables, adapters, batteries, etc. 
-  Set up food, drinks, swag, and any other materials you have for the event. 
-  Welcome your guests, share important updates with the group, review agenda, thank your host, and introduce your speakers. 
-  After the event, make sure you leave the venue clean and return any loaned AV equipment. 
-  Send a thank you to attendees and include a form for feedback and a reminder to RSVP for your next meetup (if one has been scheduled). 

### What to do the day after your event 

-  Send thank you notes to the venue hosts, your co-organizers and volunteers, the speakers, and anyone else who helped you with the meetup. 

### Simple tips to take your meetup to the next level 

- Signs, signs, everywhere signs: When setting up for your meetup, signs can be a big help to make sure your guests feel comfortable. Post a 'Welcome' sign near the door so folks know they are in the right place upon arrival and hang directional signs pointing your guests to the presentation space, refreshments, and restrooms. It also helps to post the wifi password and an agenda somewhere in the room (on paper, a whiteboard, or a welcome slide on the screen) so everyone knows the plan for the evening. 
- Set the mood: the atmosphere at a meetup tends to be set by the first guests to arrive. If they grab seats and jump on their phones, later arrivals tend to follow that lead. If you want a more lively meetup, make yourself available for conversation as guests begin to arrive and introduce guests to each other to keep the conversations going. Some background music playing at a low-volume can also help to prevent the library vibe. 
- Speaker swag: when possible, it's always great to send your speakers home with some swag as a token of your appreciation. A special sticker just for speakers at your meetup (for example: your group's logo in a different color scheme) can go a long way. 

## Resources

### Logos 

GitLab logos can be found in our [press kit](https://about.gitlab.com/press/press-kit/#logos). 

### Templates 

These [communication templates](https://drive.google.com/drive/folders/1xglxuxFcxATpQ0ZajYvYu5aIyEK7TvTK?usp=sharing) can help you get a head start on your communications with your community. Templates include sample agendas, reminder and thank you emails, and boilerplate language about GitLab for your group or event descriptions. 
 
### Signs for your meetup 

Use [these signs](https://drive.google.com/open?id=1jO-MbZI21sXbzZYQ0u4ClghmNJFzsT7X) to help your members find what they need and free up your own time for deeper conversations then directing your guests to the bathroom or pizza. 

### Feedback form

Feedback is critical. A simple [three question survey](https://docs.google.com/forms/d/167QK2Oudqrdu_hpRCK45G7WDW65IDV63XWgjKa6Srrw/edit) can help you improve your group and gives your members a chance to offer feedback.
