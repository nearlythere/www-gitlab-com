---
layout: markdown_page
title: Product Vision - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for the Manage stage of the DevOps lifecycle. If you'd 
like to discuss this vision directly with the product manager for [Manage](https://about.gitlab.com/handbook/product/categories/manage/)., 
feel free to reach out to Jeremy Watson via [e-mail](mailto:jwatson@gitlab.com), 
[Twitter](https://twitter.com/gitJeremy), or by [scheduling a video call](https://calendly.com/jeremy_/gitlab-product-chat).

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

## Overview

For administrators and executives, the process of management is always on. It extends to managing people, money, and risk; when the stakes are high, these stakeholders demand an experience and feature set that makes them feel in control. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to make software work for them.

Not only do we want to fulfill those fundamental needs, we want to give you the freedom to work in new and powerful ways. We aspire to answer questions managers didn't know they had, and to automate away the mundane.

For these users, Manage's role in GitLab is to **help organizations prosper with configuration and analytics that enables them to work more efficiently**. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

Manage also maintains and iterates on foundational building blocks of GitLab - like projects and groups - that support the rest of the application. Evolving these areas of the product is an ongoing priority for Manage, in order to ensure that we're **removing barriers to finding value and helping users effortlessly find solutions to problems that GitLab helps solve.**

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Themes for 2019

We’re realizing this vision in 2019 by delivering more powerful insights, making it easier than ever to do meaningful work in GitLab, and iterating on features that enables large organizations to thrive. We're also taking steps forward in improving traceability and security to ensure that GitLab can meet the needs of the enterprise, even in highly regulated environments. Finally, we're also improving the core experience for our end users and making existing features lovable.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KV2T4BV4RB4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 🚢 Operating at scale

As GitLab continues to grow, we want to continue to iterate and improve on existing features. This is especially true of features that help large organizations thrive in GitLab.

We’re continuing to improve on authentication within GitLab, which is of critical importance for managing users at scale. We’re continuing to build out Group SAML for GitLab.com by automating membership and permissions management. We’re also improving OAuth by allowing you to programmatically manage tokens.

We’re also excited to give instances more control and power over how they manage spending. You’ll be able to clearly understand how your instance’s license is being used, with granular control over seats. Alongside making billing easier to understand than ever, we’re also improving our billing portal to give you the power to self-serve changes to your subscription.

Lastly, GitLab’s [single application](https://about.gitlab.com/handbook/product/#single-application) approach to DevOps puts the entire software development lifecycle in a single place. As a result, users don’t have to stitch together an overly-fragmented toolchain - and we want to go deeper in on that advantage with effortless, custom workflows.

### 🔔 Analytics and insights

As instances thrive, the amount of information flowing through GitLab grows exponentially. An administrator’s job quickly becomes more reliant on automation and tools to help them stay reactive (I need to respond to an urgent request for information) and proactive (tell me about areas of risk).

We'll find new opportunities to tell you something insightful and new about how you’re shipping software. We're doubling down on the power of analytics to provide insight into how instances can reduce cycle time and ship faster. 

### 🔐 Security & compliance

“Trust, but verify”: GitLab makes it easy to contribute, but administrators should have comprehensive and consistent views on who is has done what. In short, changes in GitLab should be fully traceable. 

After code gets merged, it may involve a host of individuals, commits, and objects - while we make it easy to go from idea to production, tracing that history back should also be a cinch. Since the heart of code change in GitLab is the merge request, we’ll add the ability to see the a deep history - including the issues, people, and commits - that led to the change. 

Monitoring and traceability should be built deep into the application and allow GitLab to thrive in any regulated environment.

### ❤️ Lovability

As we continue to build on the themes above, we don’t seek to create a product that’s merely functionally complete; we want to create a GitLab that offers an unbeatable experience that our users consistently fall in love with. 

To accomplish this, we’ll be using feedback from users and data to inform how we can make the experience simpler, more accessible, and easier to understand. We’ll polish parts of GitLab that are frequently experienced - like projects, groups, and user onboarding - and iterate until we get to a frictionless experience that people love and return to.

## Priorities

For more specifics on how these themes are connected to Manage's [value statements](https://about.gitlab.com/direction/manage/#overview), see the list of specific initiatives that we're focusing on below.

The following list is intended to be prioritized from the top-down, descending in importance. The order that we work on things may not perfectly reflect the order seen here, for a variety of reasons, including:

1. We may not be able to focus the entire team on a single initiative. Working concurrently on the same thing might not be possible, so we'll have to work on multiple initiatives at once.
1. This doesn't include bugs, security issues, and tech debt.
1. Another need may arise (other teams are dependent on us completing something, we need to hit a date, etc) that forces us to prioritize outside of this framework.

### Help organizations prosper

#### Current focus

| Priority | Focus | Why? | 
| ------ | ------ | ------ |
| 1 | [SSO for GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/731) | Lack of a secure, enterprise-grade SSO for GitLab.com is the biggest barrier to more widespread adoption by large groups. Nearly all mid/large size companies have some type of centralized identity management, and supporting provisioning/deprovisioning of users and SSO enforcement for security are must-haves. These capabilities enable enterprises looking for a hosted solution to look at GitLab.com as their solution; otherwise, they're forced to consider GitHub or Bitbucket. The business opportunity for GitLab is [very strong](https://docs.google.com/spreadsheets/d/1dU1QbbSujGs3rszCZUyEqmtMKtGHBn6elFKo_IOS-KY/edit#gid=0). |
| 2 | [Smart card authentication](https://gitlab.com/groups/gitlab-org/-/epics/279) | Top ask from public sector prospects and customers (along with [FIPS compliance](https://gitlab.com/gitlab-org/gitlab-ce/issues/41463)). Strong [business opportunity](https://gitlab.com/gitlab-org/gitlab-ee/issues/726) for GitLab. Key differentiator since few or no other similar applications support smart card authentication out of the box. |

#### Next up

| Priority | Focus | Why? | 
| ------ | ------ | ------ |
| 3 | [Inactive user management](https://gitlab.com/groups/gitlab-org/-/epics/256) | Instances, especially some [noteworthy](https://na34.salesforce.com/00161000004bZxf?srPos=0&srKp=001) [customers](https://na34.salesforce.com/00161000005cxCt?srPos=0&srKp=001) have struggled with understanding which users on their instance are actually using GitLab. This should be an easy question to answer. |
| 4 | [Comprehensive audit log](https://gitlab.com/groups/gitlab-org/-/epics/736) | Customers operating in regulated environments and [large enterprises](https://na34.salesforce.com/0016100000KvaIg?srPos=1&srKp=001) want a comprehensive, reliable source of truth to use as a single source of truth. We should be able to offer a comprehensive record of what happened and when to make event investigation possible. Without this, some customers feel vulnerable and frustrated by questions that GitLab's audit events/logs can't answer. |
| 5 | [Better access control for groups](https://gitlab.com/groups/gitlab-org/-/epics/84) | Managing access control at scale is a challenge for most large instances; in GitLab, we manage this with groups, which we need to continue to improve on. Organizations still script around common access control tasks, and we should be able to establish access restrictions at the group level like domain/IP whitelisting. This is especially important for GitLab.com, where users can accidentally be added to private groups. |
| 6 | [Analytics](https://gitlab.com/groups/gitlab-org/-/epics/164) [improvements](https://gitlab.com/groups/gitlab-org/-/epics/39) | Cycle analytics and ConvDev Index are important analytics tools for understanding cycle time and your instance's DevOps maturity, but both need to be improved. We need to fix Cycle Analytics' stage calculations and be able to use it at the group/instance level, and we should refactor ConvDev Index into a more useful DevOps Score that's actionable and demonstrating clear value. |
| 7 | [Help instances manage seat utilization](https://gitlab.com/groups/gitlab-org/-/epics/284) | Instances want to know how many seats their instance is using, but this isn't easy to understand. We should make it simple to understand how many seats an instance is using, how that's changed over time, and which users were added. |

### Remove barriers to finding value

#### Current focus

| Priority | Focus | Why? | 
| ------ | ------ | ------ |
| 1 | [New onboarding](https://gitlab.com/groups/gitlab-org/-/epics/611) | Onboarding is a strong opportunity for GitLab. Strong onboarding experiences set users up for success and drive retention/revenue by encouraging users to find a magic moment in the product and "stick" to using it. We should develop a strong onboarding experience that introduces a user to the essentials of GitLab, across stages. |
| 2 | [Smart dashboards](https://gitlab.com/groups/gitlab-org/-/epics/365) | GitLab is a wide application with no well-defined "entry point" that spans across stages. The default page is the project dashboard, which tells a user little about what's been going on since their last visit. To help users be more productive in GitLab and explore features across stages, we should define an entry point to the application that tells a user "what they need to know". |
| 3 | Redesigned [project](https://gitlab.com/groups/gitlab-org/-/epics/755)/[group](https://gitlab.com/groups/gitlab-org/-/epics/571) presentation | Projects and groups are both first-class citizens in GitLab. Both are fundamental to using our application, and both should be enjoyable to use. We'll iterate on user feedback and ensure that we're presenting project and group information users want to see the most. | 

#### Next up

| Priority | Focus | Why? | 
| ------ | ------ | ------ |
| 4 | [Importer improvements](https://gitlab.com/groups/gitlab-org/-/epics/801) | GitLab usage frequently starts with an import from somewhere else. Our importers should be resistant to error and recover gracefully from failure. We should also make large-scale imports easier to manage. |
| 5 | [New importers](https://gitlab.com/groups/gitlab-org/-/epics/800) | We should continue extending our importers, supporting applications that are in high-demand from our users like Phabricator and Trello. |

## Subject labels

For a more comprehensive view of where we're heading, Manage is primarily responsible for the labels below. Each label links to a vision epic, which details our goals and plan for the specified area of GitLab. As always, we'd love your feedback - feel free to leave comments in the respective epic.

| Label | Description |
| ------ | ------ |
| [subscriptions](https://gitlab.com/groups/gitlab-org/-/epics/551) | Our [customers portal](https://customers.gitlab.com/) is how users subscribe and pay for GitLab. |
| [license](https://gitlab.com/groups/gitlab-org/-/epics/600) | Our licensing model for GitLab EE. This is how we provide and maintain licenses to self-managed customers; also covers our seat model and how we count users. | 
| [authentication](https://gitlab.com/groups/gitlab-org/-/epics/628) | How users register with GitLab, log in, and maintain these credentials over time. Also includes the authorization, ldap, saml, and oauth labels. | 
| [groups](https://gitlab.com/groups/gitlab-org/-/epics/587) | Like directories in a filetree, groups are used to organize many projects and people. | 
| [navigation](https://gitlab.com/groups/gitlab-org/-/epics/610) | Specific features have their own navigation, but this label covers common navigation elements across GitLab like the sidebar and topbar. It also covers some features and flows that are shared across stages, like the Activity page. | 
| [user profile](https://gitlab.com/groups/gitlab-org/-/epics/649) | Covers both the personal settings page at `/profile` and how we present that [information on an individual](https://gitlab.com/jeremy) to others. | 
| [projects](https://gitlab.com/groups/gitlab-org/-/epics/646) | Anything related to projects in GitLab, but not including the repository itself or git-specific functionality. | 
| [analytics](https://gitlab.com/groups/gitlab-org/-/epics/647) | Analytics features that span multiple areas of the product: Cycle Analytics, Contribution Analytics, and ConvDev Index (DevOps Score/Maturity). Does not include feature-specific analytics like Issue Analytics. | 
| [audit events](https://gitlab.com/groups/gitlab-org/-/epics/642) | Tracking events in GitLab for compliance and oversight. Our goal is to have 100% of activity in GitLab be recorded and auditable. | 
| [importers](https://gitlab.com/groups/gitlab-org/-/epics/648) | Getting data in and out of GitLab. Includes importing (bringing information into GitLab from a GitLab project export or other application) and exporting. Also includes the `project import` and `project export` labels. | 
| [admin dashboard](https://gitlab.com/groups/gitlab-org/-/epics/651) | Configuration and presentation in the admin panel (`/admin`). | 
| [permissions](https://gitlab.com/groups/gitlab-org/-/epics/599) | Our user-based permissioning model; how we configure and decide which users are allowed to do what. | 
| [user management](https://gitlab.com/groups/gitlab-org/-/epics/603) | Managing the number of active users who are using an instance, identifying who they are, and making changes to their status on the instance. | 
| [gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/537) | Functionality specific to the operation and success of GitLab.com. | 
| [internationalization](https://gitlab.com/groups/gitlab-org/-/epics/650) | Translating GitLab into other languages. | 

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Manage can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Amanage), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

You can see further details on the prioritization and development process on the [page for the Manage team](https://about.gitlab.com/handbook/product/categories/manage/).

## Direction issues

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
