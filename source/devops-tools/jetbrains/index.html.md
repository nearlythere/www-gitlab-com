---
layout: markdown_page
title: "JetBrains TeamCity"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Team City Java-based build management and continuous integration server from JetBrains.  They are widely know for their extended family of integrated development environments (IDEs) for SQL and the programming languages Java, Kotlin, Ruby, Python, PHP, Objective-C, C++, C#, Go and JavaScript.  Team City is known for its ease of configuration, reliability and GitHub configuration. 

In contrast, GitLab already provides more than what TeamCity does with CI only, by providing a fully integrated single application for the entire DevOps lifecycle. More than TeamCity, GitLab also provides planning, SCM, packaging, release, configuration, and monitoring (in addition to the CI TeamCity is focused on).
## Resources
* [JetBrains](https://www.jetbrains.com/teamcity/?fromMenu)
* [JetBrains Wikipedia](https://en.wikipedia.org/wiki/JetBrains)

