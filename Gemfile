# If you do not have OpenSSL installed, change
# the following line to use 'http://'
source 'https://rubygems.org'

# For faster file watcher updates on Windows:
gem 'wdm', '~> 0.1.0', platforms: %i[mswin mingw x64_mingw]

# Windows does not come with time zone data
gem 'tzinfo-data', platforms: %i[mswin mingw x64_mingw jruby]

# Middleman Gems
gem 'middleman', '~> 4.2'
gem 'middleman-blog', '~> 4.0'
gem 'middleman-livereload'
gem 'middleman-minify-html'
gem 'middleman-autoprefixer', '~> 2.7.1'
gem 'middleman-syntax'

gem 'kramdown', '~> 1.10'
gem 'nokogiri', '>= 1.8.3'
gem 'sassc'
gem 'stringex'
gem 'countries'

# Replacement of therubyracer
gem 'mini_racer', '~> 0.2'

# For feed.xml.builder
gem 'builder', '~> 3.0'

# Direction generation
gem 'faraday', '>= 0.15.0'
gem 'faraday_middleware'
gem 'faraday_middleware-parse_oj', '~> 0.3'

# Handbook CHANGELOG generation
gem 'gitlab'

group :development, :test do
  gem 'html-proofer'
  gem 'docopt'
  gem 'scss_lint', require: false
  gem 'rspec', '~> 3.5', require: false
  gem 'rubocop', '~> 0.59.0', require: false
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'selenium-webdriver'
end
