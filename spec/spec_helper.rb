$LOAD_PATH << File.expand_path('..', __dir__)

require 'middleman-core'
require 'middleman-core/rack'

require 'middleman-autoprefixer'
require 'middleman-blog'
require 'middleman-syntax'

require 'lib/homepage'
require 'lib/changelog'

require 'spec/support/capybara'

RSpec.configure do |config|
  config.define_derived_metadata(file_path: %r{/spec/browser}) do |metadata|
    metadata[:type] = :feature
  end

  # Really slow, only do it once, and only when needed
  config.before(:all, type: :feature) do
    root = File.expand_path(File.join(File.dirname(__FILE__), ".."))
    Middleman::Logger.singleton(Logger::WARN, false)

    Capybara.app = Middleman::Rack.new(Middleman::Application.new).to_app do
      set :root, root
      set :environment, :test
    end

    Capybara.save_path = "tmp/capybara"
    Capybara::Screenshot.instance_variable_set(:@capybara_root, File.expand_path(Capybara.save_path, root))
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.order = :random
  Kernel.srand config.seed
end
